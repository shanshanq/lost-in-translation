export const ACTION_SESSION_SET = "[session]SET";
export const ACTION_SESSION_LOGOUT = "[session] LOGOUT";
export const ACTION_SESSION_CLEAR = "[session] CLEAR";

export const sessionSetAction = (profile) => ({
  type: ACTION_SESSION_SET,
  payload: profile,
});

export const sessionLogoutAction = () => ({
  type: ACTION_SESSION_LOGOUT,
});

export const sessionClearAction = () => ({
  type: ACTION_SESSION_CLEAR,
});
