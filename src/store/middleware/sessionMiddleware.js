import {
  ACTION_SESSION_SET,
  ACTION_SESSION_LOGOUT,
  ACTION_SESSION_CLEAR,
  sessionClearAction,
} from "../actions/sessionActions";

export const sessionMiddleware =
  ({ dispatch }) =>
  (next) =>
  (action) => {
    next(action);

    if (action.type === ACTION_SESSION_SET) {
      localStorage.setItem("translator-ss", JSON.stringify(action.payload));
    }
    if (action.type === ACTION_SESSION_CLEAR) {
      localStorage.removeItem("translator-ss");
    }
    if (action.type === ACTION_SESSION_LOGOUT) {
      dispatch(sessionClearAction());
    }
  };
