import "./App.css";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import Login from "./components/Login/Login";
import NotFound from "./components/NotFound/NotFound";
import Translation from "./components/Translation/Translation";
import Profile from "./components/Profile/Profile";
import Navbar from "./components/Navbar/Navbar";
import AppContainer from "./hoc/AppContainer";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Navbar />
        <Switch>
          <Route path="/" exact component={Login}></Route>
          <Route path="/translation" exact component={Translation} />
          <Route path="/profile" exact component={Profile} />
          <Route path="*" component={NotFound}></Route>
        </Switch>
        <AppContainer>
          <footer class=" py-3 my-4 border-top mt-5">
            <div class="container-fluid ">
              <span class="mb-0 text-muted">© 2021 Shanshan Qu</span>
            </div>
          </footer>
        </AppContainer>
      </div>
    </BrowserRouter>
  );
}

export default App;
