import AppContainer from "../../hoc/AppContainer";
import { useSelector, useDispatch } from "react-redux";
import { useState, useEffect } from "react";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { sessionLogoutAction } from "../../store/actions/sessionActions";
import { useHistory } from "react-router-dom";
import { TranslateAPI } from "../Translation/TranslateAPI";
import { ProfileAPI } from "./ProfileAPI";
import "./Profile.css";

const Profile = () => {
  const history = useHistory();
  const mySwal = withReactContent(Swal);
  const dispatch = useDispatch();
  const { username, id } = useSelector((state) => state.sessionReducer);
  const [latest, setLatest] = useState({
    translations: [],
  });
  useEffect(() => {
    // Get the latest translations
    TranslateAPI.getTranslations(id)
      .then((res) => {
        setLatest({
          translations: res.translations,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }, [latest]);

  const logout = () => {
    mySwal
      .fire({
        backdrop: true,
        denyButtonText: "Cancel",
        showCancelButton: true,
        confirmButtonColor: "var(--bs-yellow)",
        cancelButtonColor: "var(--bs-teal)",
        title: <p>Logout</p>,
        text: "Are you sure you want to log out?",
        cancelButtonText: "Nope",
        confirmButtonText: "Yes",
      })
      .then((result) => {
        if (result.isConfirmed) {
          //clear the storage and return to the start page
          dispatch(sessionLogoutAction());
          history.push("/");
        }
      });
  };

  const clear = () => {
    ProfileAPI.clearData(id)
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        console.log(error.message);
      });
  };

  return (
    <>
      <AppContainer>
        <div className="title mb-5">
          <p>Welcome to your profile, {username}</p>
        </div>
        <button type="button" className="btn btn-warning" onClick={logout}>
          Log out
        </button>
        <button type="button" className="btn btn-warning " onClick={clear}>
          Clear the translations
        </button>
        <p className="mt-5">Latest Translations</p>
        <ul className="list-group">
          {latest.translations.map((str, index) => {
            return (
              <li className="list-group-item" key={index}>
                {str}
              </li>
            );
          })}
        </ul>
      </AppContainer>
    </>
  );
};

export default Profile;
