export const ProfileAPI = {
  // Clear the translations stored
  clearData(id) {
    return fetch(`http://localhost:8080/data/${id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ translations: [] }),
    }).then(async (response) => {
      if (!response.ok) {
        const { error = "Error occurred with API request" } =
          await response.json();
        throw new Error(error);
      }
      console.log("RESPONSE: " + response.body);
      return response.json();
    });
  },
};
