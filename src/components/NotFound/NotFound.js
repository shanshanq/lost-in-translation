import { Link } from "react-router-dom";
import AppContainer from "../../hoc/AppContainer";
import Navbar from "../../components/Navbar/Navbar";

const NotFound = () => {
  return (
    <>
      <Navbar />
      <AppContainer>
        <h4>404 NOT FOUND</h4>
        <Link to="/">Back to Login page</Link>
      </AppContainer>
    </>
  );
};

export default NotFound;
