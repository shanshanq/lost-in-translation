import "./Login.css";
import logo from "../../assets/img/Logo.png";
import AppContainer from "../../hoc/AppContainer";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loginAttemptAction } from "../../store/actions/loginActions";
import { Redirect } from "react-router-dom";

const Login = () => {
  const dispatch = useDispatch();
  const { loginAttempting, loginError } = useSelector(
    (state) => state.loginReducer
  );
  const { loggedIn } = useSelector((state) => state.sessionReducer);

  const [data, setData] = useState({
    username: "",
    translations: [],
  });

  const onInputChange = (event) => {
    setData({
      ...data,
      [event.target.id]: event.target.value,
    });
  };
  const onSubmit = (event) => {
    event.preventDefault();
    dispatch(loginAttemptAction(data));
  };

  return (
    <>
      {loggedIn && <Redirect to="/translation" />}
      {!loggedIn && (
        <>
          <AppContainer>
            <div className="intro mb-5">
              <img src={logo} alt="logo" />
              <h1>Lost in Translation</h1>
              <p>Get started</p>
            </div>
            <div class="input-group input-group-lg">
              <span
                className="material-icons input-group-text"
                id="inputGroup-sizing-sm"
              >
                keyboard
              </span>
              <input
                type="text"
                id="username"
                class="form-control"
                placeholder="What's your name?"
                onChange={onInputChange}
              ></input>
              <button
                class="btn btn-outline-secondary"
                type="button"
                onClick={onSubmit}
              >
                Go
              </button>
            </div>
            {loginAttempting && (
              <div class="d-flex align-items-center">
                <div
                  class="spinner-border spinner-border-sm m-2 d-block"
                  role="status"
                  aria-hidden="true"
                ></div>
              </div>
            )}
          </AppContainer>
        </>
      )}
    </>
  );
};

export default Login;
