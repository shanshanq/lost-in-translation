import { Link, NavLink } from "react-router-dom";
import AppContainer from "../../hoc/AppContainer";
import { useSelector } from "react-redux";

const Navbar = () => {
  const { username } = useSelector((state) => state.sessionReducer);
  return (
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
      <AppContainer>
        <Link className="navbar-brand" to="/">
          <h2>Lost In Translation</h2>
        </Link>
        {username && (
          <>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#mainNavbar"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div id="profile">
              <ul className="navbar-nav ml-auto">
                <li className="nav-item">
                  <NavLink className="nav-link d-flex" to="/profile">
                    <span className="material-icons">account_circle</span>
                    &nbsp;Welcome, {username}
                  </NavLink>
                </li>
              </ul>
            </div>
          </>
        )}
      </AppContainer>
    </nav>
  );
};

export default Navbar;
