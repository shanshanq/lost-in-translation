import AppContainer from "../../hoc/AppContainer";
import { useState, useEffect } from "react";
import "./Translation.css";
import { useSelector } from "react-redux";
import { TranslateAPI } from "./TranslateAPI";

const Translation = () => {
  const { id } = useSelector((state) => state.sessionReducer);
  const urls = [
    { key: "a", image: "https://i.imgur.com/9xaQ5sQ.png" },
    { key: "b", image: "https://i.imgur.com/yCPemA1.png" },
    { key: "c", image: "https://i.imgur.com/eI2752S.png" },
    { key: "d", image: "https://i.imgur.com/rdDekMP.png" },
    { key: "e", image: "https://i.imgur.com/Dr6uoOu.png" },
    { key: "f", image: "https://i.imgur.com/fBYOzWg.png" },
    { key: "g", image: "https://i.imgur.com/bDNM3V2.png" },
    { key: "h", image: "https://i.imgur.com/OBT9E5F.png" },
    { key: "i", image: "https://i.imgur.com/TYru4jK.png" },
    { key: "j", image: "https://i.imgur.com/63a0USQ.png" },
    { key: "k", image: "https://i.imgur.com/fE94E5E.png" },
    { key: "l", image: "https://i.imgur.com/bWE33CL.png" },
    { key: "m", image: "https://i.imgur.com/hXyciGV.png" },
    { key: "n", image: "https://i.imgur.com/nP4PlHm.png" },
    { key: "o", image: "https://i.imgur.com/u9q3cWx.png" },
    { key: "p", image: "https://i.imgur.com/T3y1VfH.png" },
    { key: "q", image: "https://i.imgur.com/Mkuyrxk.png" },
    { key: "r", image: "https://i.imgur.com/ZenkQXW.png" },
    { key: "s", image: "https://i.imgur.com/sRdcmqg.png" },
    { key: "t", image: "https://i.imgur.com/hvItNC8.png" },
    { key: "u", image: "https://i.imgur.com/qArvTCo.png" },
    { key: "v", image: "https://i.imgur.com/28ELI3z.png" },
    { key: "w", image: "https://i.imgur.com/dsFaeiT.png" },
    { key: "x", image: "https://i.imgur.com/GyHvvhs.png" },
    { key: "y", image: "https://i.imgur.com/ffpjCwB.png" },
    { key: "z", image: "https://i.imgur.com/DSgXTX1.png" },
  ];
  const [result, setResult] = useState({
    str: "",
    display: false,
  });
  const [history, setHistory] = useState({
    translations: [],
  });

  useEffect(() => {
    // Get the current translations made by current user
    TranslateAPI.getTranslations(id)
      .then((res) => {
        setHistory({
          translations: res.translations,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }, [result]);

  const onInputChange = (e) => {
    setResult({
      ...result,
      display: false,
      [e.target.id]: e.target.value,
    });
  };

  const translate = () => {
    // Set the text string state and get it ready to display the corresponding images
    setResult({
      str: result.str,
      display: true,
    });
    // Store this translation to DB
    TranslateAPI.translate(id, {
      translations: [...history.translations, result.str],
    }).then((res) => console.log(res));
  };

  return (
    <AppContainer>
      <main>
        <p className="mb-5">Translation</p>
        <div className="input-group input-group-lg">
          <span
            className="material-icons input-group-text"
            id="inputGroup-sizing-sm"
          >
            keyboard
          </span>
          <input
            type="text"
            id="str"
            className="form-control"
            onChange={onInputChange}
          ></input>
          <button
            className="btn btn-outline-secondary"
            type="button"
            onClick={translate}
          >
            Translate
          </button>
        </div>
        <div className="card mt-3">
          <div className="card-body">
            {!result.display && (
              <span>
                Please hit the translate button to view the translation!
              </span>
            )}

            {result.display &&
              result.str
                .toLowerCase()
                .split(" ")
                .join("")
                .split("")
                .map((c, index) => {
                  let imgs = urls.filter((x) => c === x.key)[0];
                  return <img src={imgs.image} alt={c} />;
                })}
          </div>
        </div>
      </main>
    </AppContainer>
  );
};

export default Translation;
