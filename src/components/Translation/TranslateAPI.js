export const TranslateAPI = {
  // Update the translation history array in the DB after the translation
  translate(id, str) {
    return fetch(`http://localhost:8080/data/${id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(str),
    }).then(async (response) => {
      if (!response.ok) {
        const { error = "Error occurred with API request" } =
          await response.json();
        throw new Error(error);
      }
      console.log("RESPONSE: " + response.body);
      return response.json();
    });
  },

  // Get the translations
  getTranslations(id) {
    return fetch(`http://localhost:8080/data/${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    }).then(async (response) => {
      if (!response.ok) {
        const { error = "Error occurred with API request" } =
          await response.json();
        throw new Error(error);
      }
      console.log("RESPONSE: " + response.body);
      return response.json();
    });
  },
};
