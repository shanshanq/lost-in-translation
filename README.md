# Simple sign language translator App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Screens
1. The Startup Page
2. The Translation Page
3. The Profile Page

## Available Scripts
In the project directory, you can run the following scripts.

First, you need to run the json server on port 8080:
### `json-server --watch db.json -p 8080`

### `npm install`

### `npm start`
